({
	checkValidity : function(cmp, helper) {
		if(cmp.get('v.disabled')){
			return helper.setValidity(cmp, helper, true, '');
		}

		//var value = cmp.find('inputText').getElement().value;	
		var value = cmp.get('v.value');

		var required = cmp.get('v.required');
		if(required && (value == null || value === '')){
			return helper.setValidity(cmp, helper, false, 'Complete this field');
		}

		var pattern = cmp.get('v.pattern');
		if(pattern != null){
			var reqEx = new RegExp(pattern);

			if(!(reqEx.test(value))){
				return helper.setValidity(cmp, helper, false, cmp.get('v.messageWhenPatternMismatch'));
      }
		}

		return helper.setValidity(cmp, helper, true, '');
	},

	setValidity : function(cmp, helper, isValid, validityMsg){
		cmp.set('v.validity', {'valid' : isValid});
		cmp.set('v.errorMsg', validityMsg);

		if(isValid){
			$A.util.removeClass(cmp.find('divParent'), "slds-has-error");
		}else{
			$A.util.addClass(cmp.find('divParent'), "slds-has-error");
		}

		return isValid;
	}
})