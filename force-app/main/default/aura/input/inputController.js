({
	handleValueChanged : function(cmp, event, helper){
		if(cmp.get('v.value') === undefined){
			cmp.set('v.value', null);
		}else{
			helper.checkValidity(cmp, helper);
		}
	},

	handleCheckInputValidity : function(cmp, event, helper){
		return helper.checkValidity(cmp, helper);
	},

	onKeyUpInput : function(cmp, event, helper){
		var inputText = cmp.find('inputText').getElement().value;
		cmp.set('v.value', inputText);
	},

	onBlurHandler : function(cmp, event, helper){
		//helper.checkValidity(cmp, helper);
	}
})
