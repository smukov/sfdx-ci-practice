({
	scriptsLoaded : function(cmp, event, helper){
		cmp.set('v.scriptsLoaded', true);
		helper.init(cmp, helper);
	},

	init : function(cmp, event, helper) {
		if(cmp.get('v.stackPills')){
			$A.util.removeClass(cmp.find('ulPills'), 'slds-listbox_inline');
		}
		if(cmp.get('v.hidePillsBorder')){
			$A.util.addClass(cmp.find('ulPills'), 'ms-hide-pills-border');
		}

		helper.init(cmp, helper);
	},

	handleReset : function(cmp, event, helper){
		//I needed this because I'm recycling the component in modal dialog
		cmp.set('v.pills', []);
		cmp.set('v.valueDataLoaded', false);
		cmp.find('inputText').getElement().value = '';
		cmp.set('v.isInputValid', true);
	},

	onKeyUpInput : function(cmp, event, helper){
		var delimiter = cmp.get('v.delimiter');
		var inputText = cmp.find('inputText').getElement();
		var currentInput = inputText.value;
		var acceptInput = true;

		if(currentInput[currentInput.length -1] === delimiter || event.keyCode === 13){
			var values = currentInput.split(delimiter);

			if(cmp.get('v.preventInvalidInput') === true){
				for(var i = 0; i < values.length; i++){
					if(helper.isInputValid(cmp, helper, values[i].trim()) === false){
						acceptInput = false;
						break;
					}
				}
			}

			if(acceptInput === true){
				helper.addNewPills(cmp, helper, values);
				inputText.value = '';
			}
		}

		cmp.set('v.isInputValid', acceptInput);
	},

	handlePillsChanged : function(cmp, event, helper){
		helper.parsePillsToField(cmp, helper);
	},

	handleValueChanged : function(cmp, event, helper){
		if(cmp.get('v.valueDataLoaded') === false && cmp.get('v.scriptsLoaded') === true && cmp.get('v.value') !== null){
			cmp.set('v.valueDataLoaded', true);
			helper.parseFieldToPills(cmp, helper);
		}
	},

	handleIsInputValidChange : function(cmp, event, helper){
		var divErrorMessage = cmp.find('divErrorMessage');
		var divContainer = cmp.find('divContainer');
		if (cmp.get('v.isInputValid')){
			$A.util.removeClass(divContainer, "slds-has-error");
			$A.util.addClass(divErrorMessage, "slds-hide");
		}else{
			$A.util.addClass(divContainer, "slds-has-error");
			$A.util.removeClass(divErrorMessage, "slds-hide");
		}
	},

	onClickPill : function(cmp, event, helper){
		//NOTE: might be implemented at one point
	},

	onRemovePill : function(cmp, event, helper){
		var pillId = event.getSource().get('v.name');
		var pills = cmp.get('v.pills');

		for(var i = 0; i < pills.length; i++){
			if(pillId === pills[i].id){
				pills.splice(i, 1);
				break;
			}
		}

		cmp.set('v.pills', pills);
	}
})