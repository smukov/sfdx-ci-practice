({
	init : function(cmp, helper) {
		if(cmp.get('v.scriptsLoaded') === true){
			helper.parseFieldToPills(cmp, helper);
		}
	},

	addNewPills : function(cmp, helper, values){
		var pills = cmp.get('v.pills');

		for(var i = 0; i < values.length; i++){
			var trimmedVal = values[i].trim();
			if(trimmedVal !== ""){
				pills.push({
					id : lexUtil.guidGenerator(),
					label : trimmedVal,
					isValid : helper.isInputValid(cmp, helper, trimmedVal)
				});
			}
		}

		cmp.set('v.pills', pills);
	},

	isInputValid : function(cmp, helper, value){
		return regexUtil.validateInput(cmp.get('v.validationTypes'), value);
	},

	parsePillsToField : function(cmp, helper){
		var pills = cmp.get('v.pills');
		var delimiterInDatabase = cmp.get('v.delimiterInDatabase');
		var fieldStr = '';

		for(var i = 0; i < pills.length; i++){
			//NOTE: I think that it's smarter to allow saving to database //if(pills[i].isValid)
			fieldStr += pills[i].label + delimiterInDatabase;
		}

		try{
			cmp.set('v.value', fieldStr);
		}catch(e){
			//ignore issue that occurs when trying to set unbinded value
		}
	},

	parseFieldToPills : function(cmp, helper){
		var fieldStr = cmp.get('v.value');
		var delimiterInDatabase = cmp.get('v.delimiterInDatabase');
		var pills = [];
		var splitFieldStr = [];
		if(fieldStr != null){
			splitFieldStr = fieldStr.split(delimiterInDatabase);
		}

		for(var i = 0; i < splitFieldStr.length; i++){
			if(splitFieldStr[i] !== ""){
				pills.push({
					id : lexUtil.guidGenerator(),
					label : splitFieldStr[i],
					isValid : helper.isInputValid(cmp, helper, splitFieldStr[i])
				});
			}
		}

		cmp.set('v.pills', pills);
	}
})
