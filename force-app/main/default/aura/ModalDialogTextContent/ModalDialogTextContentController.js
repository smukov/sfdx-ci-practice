({
	save : function(cmp, event, helper) {

		var srsEvent = cmp.getEvent('closeTextDialogEvent');
		srsEvent.setParam('payload', {
			target: cmp.get('v.target'),
			answer: true
		});
		srsEvent.fire();

		cmp.getEvent("closeDialogEvent").fire();
	},

	close : function(cmp, event, helper) {
		cmp.getEvent("closeDialogEvent").fire();
	}
})