({
	showDialog : function(cmp, event, helper) {
		helper.toggleDialog(cmp, true);
		if(cmp.get('v.body')[0].prepare != null){
			cmp.get('v.body')[0].prepare(event.getParam('arguments').payload);
		}
	},

	handleCloseDialogEvent : function(cmp, event, helper){
		event.stopPropagation();
		helper.toggleDialog(cmp, false);
	},

	btnClose : function(cmp, event, helper){
		cmp.get('v.body')[0].close();
	},

	btnSave : function(cmp, event, helper){
		cmp.get('v.body')[0].save();
	}
})