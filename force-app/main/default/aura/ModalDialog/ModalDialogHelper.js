({
	toggleDialog : function(cmp, show) {
		if(show){
			$A.util.removeClass(cmp.find('divDialog'), "slds-hide");
		}else{
			$A.util.addClass(cmp.find('divDialog'), "slds-hide");
		}
	}
})