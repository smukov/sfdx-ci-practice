({
	setActionCallback : function(cmp, helper, action, success){
		return action.setCallback(this, function(response){
			$A.util.addClass(cmp.find("spinner"), "slds-hide");
			var state = response.getState();
			if(state === "SUCCESS"){
				success(response);
			}else if (state === "INCOMPLETE"){
				helper.showToastMessage("warning", "incomplete: " + response.getReturnValue(), "");
      }
      else if (state === "ERROR"){
        var errors = response.getError();
        if (errors) {
          if (errors[0] && errors[0].message) {
						helper.showToastMessage("error", "Error message: " + errors[0].message, "");
          }
        } else {
					helper.showToastMessage("error", "Unknown error", "");
        }
      }
		});
	},

	showToastMessage : function(type, title, description) {
		$A.get("e.c:ShowToastEvent")
			.setParams({
				type : type,
				title : title,
				description : description,
				delay : 5000
			})
			.fire();
	},

	navigateToUrl : function(url){
		var urlEvent = $A.get("e.force:navigateToURL");
		urlEvent.setParams({
			"url": url
		});
		urlEvent.fire();
	},

	refreshPage : function(){
		$A.get('e.force:refreshView').fire();
	}
})